from pyslack import SlackHandler
import requests
import json
# chatroom codes
cssu = 'C0450BHGS'
test = 'C045SKBGP'
general = 'C044V43CY'
token = 'xoxb-4189469263-GcZXltenDra3xvA2RMcVWuae'
payload = {'token': token}
r = requests.get('https://slack.com/api/rtm.start', params=payload)
url = 'https://slack.com/api/'
post_message = url + 'chat.postMessage'
# for messages in chatroom
history = url + 'channels.history'


def send_message(text,
                 username='cssu__orders',
                 token=payload['token'],
                 channel=test):
    '''
    Send a message with text to channel under username
    '''
    args = {'token': token, 'channel': channel, 'text': text,
            'username': username, 'unfurl_links': 'true'}
    x = requests.post(post_message, params=args)


def get_history(token=payload['token'], channel=test, oldest=0):
    '''
    return json object of 20 messages in channel, starting from oldest.
    '''
    args = {'token': token, 'channel': channel,
            'count': 20, 'oldest': oldest}
    w = requests.get(history, params=args)
    j = json.dumps(w.text)
    f = json.loads(j)
    # idk why i did this and at this point i dont wanna change it...
    f = json.loads(f)
    return f


def order(text):
    # ':cssuorders: 2 chocolate ice cream sandwiches to 2270'
    x = text.split('orders:')[1]
    # '2 chocolate ice cream sandwiches to 2270'
    return x


def infile(string, file):
    for line in file:
        if string in line:
            return False
    return True


class Clique:
    '''
    A class for orders made to the CSSU via slack
    comment id: str, a comment's timestamp from slack
    message: a message sent to slack
    '''
    def __init__(self):
        ''' (self) -> Nonetype
        self.orders : list of tuples of (str, user id)
        self.last_comment: the last timestamp
        '''
        self.orders = []
        self.last_time = None
        self.possible_responses = ['order confirmed', 'confirm order']

    def make_order(self, messages, keyword=':cssuorders:'):
        ''' (self, json object) -> bool
        find messages that start with keyword
        '''
        # channel is currently set to test, should be food or general
        for message in messages:
            if message['text'].split()[0] == keyword:
                items = order(message['text'])
                reply = 'hey <@' + message['user'] + '>. You ordered ' + \
                        str(items) + '. type "order confirmed" to confirm'
                self.orders.append((items, message['user']))
                self.last_time = message['ts']
                send_message(text=reply, channel=test)
                return True
        return False

    def confirm_order(self, messages):
        time = []
        for message in messages:
            time.append(message['ts'])
            m = message['text'].lower()
            # check if the user responded w/ any variant of order confirmed
            if any([x == m for x in self.possible_responses]):
                x = -1
                for i in range(len(self.orders)):
                    if self.orders[i][1] == message['user']:
                        # channel should be cssu
                        delivery = '<@' + str(self.orders[i][1]) + \
                                   '> ordered' + self.orders[i][0]
                        send_message(channel=test, text=delivery)
                        # channel should be food or general
                        soon = 'your order will arrive soon'
                        send_message(channel=test, text=soon)
                        x = i
                self.orders.pop(x) if len(self.orders) > 0 else 1
        self.last_time = time[-1] if len(time) > 0 else 1

if __name__ == '__main__':
    from time import sleep
    c = Clique()
    while True:
        h = get_history(oldest=c.last_time, channel=test)
        messages = h['messages']
        if c.make_order(messages):
            print('maybe')
            c.confirm_order(messages)
            sleep(5)
